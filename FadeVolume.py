'''    
	FadeVolume - Fades the volume to a desired percentage
    Copyright (C) 2020  Enrico Ferrari

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import os
from subprocess import Popen, PIPE
import time

# Function to be used in other programs
def fadeVolume(Px):

	# Get volume percentage
	stdout = Popen("amixer | grep 'Front Left: Playback'", shell = True, stdout = PIPE).stdout
	output = stdout.read()

	y = str(output)

	# Remove unnecessary text, keep value
	y = y[y.find('[') + 1:]
	y = y[:y.find(']') - 1]

	# Convert from string to float
	y = float(y)

	# Fade volume while far from desired value
	while(y > Px + 1 or y < Px - 1):

		time.sleep(0.1)

		if(y > Px + 1):
			y = y - 1
		else:
			y = y + 1
		os.system('amixer -q -D pulse sset Master ' + str(y) + '%')
	
	# Set actual value once close to it
	os.system('amixer -q -D pulse sset Master ' + str(Px) + '%')
	return
