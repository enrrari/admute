'''
	admute - Mute computer while Spotify ads play
	Copyright (C) 2020	Enrico Ferrari

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import os
import dbus
from FadeVolume import fadeVolume
from subprocess import Popen, PIPE
import time

save = 0
saved = 0
n = 0
loop = "true"

# Start Spotify (Spotify closes if program quits)
#os.system("spotify &")
#time.sleep(3)

#Use dbus to get information on the song playing
SESSION_BUS = dbus.SessionBus()
try:
	SPOTIFY_BUS = SESSION_BUS.get_object("org.mpris.MediaPlayer2.spotify",
									 "/org/mpris/MediaPlayer2")
except:
	print("Spotify is not open, cannot run")
	exit()
SPOTIFY_PROPERTIES = dbus.Interface(SPOTIFY_BUS,
									"org.freedesktop.DBus.Properties")

# Play music if Spotify is paused

os.system("dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Play")

while loop == "true":
	try:
		#Get metadata from Spotify
		METADATA = SPOTIFY_PROPERTIES.Get("org.mpris.MediaPlayer2.Player",
									  "Metadata")
		PLAYBACK = SPOTIFY_PROPERTIES.Get("org.mpris.MediaPlayer2.Player",
									  "PlaybackStatus")
	except:
		print("Cannot run without Spotify open")
		exit()

	# Quits the program if Spotify is paused for a minute
	while PLAYBACK == "Paused":
		if n == 0:
			print(PLAYBACK)
		time.sleep(1)
		n = n + 1
		if n >= 60:
			print("Quit")
			exit()
		PLAYBACK = SPOTIFY_PROPERTIES.Get("org.mpris.MediaPlayer2.Player",
									  "PlaybackStatus")

#	if saved == 0:
#		save = METADATA['xesam:title']
#		saved = 1
	if METADATA['xesam:title'] != save:
		saved = 0
		os.system('clear')
		print(METADATA['xesam:title'])
		for i in range(len(METADATA['xesam:artist'])):
			print(METADATA['xesam:artist'][i])
	if saved == 0:
		save = METADATA['xesam:title']
		saved = 1

	# If an ad is playing, set computer volume to 0%
	if METADATA['xesam:title'] == "Advertisement" or METADATA['xesam:title'] == "Spotify":

		# "Beep" when ad plays
		print("\a")

		# Get volume percentage
		stdout = Popen("amixer | grep 'Front Left: Playback'", shell = True, stdout = PIPE).stdout
		output = stdout.read()

		y = str(output)

		# Remove unnecessary text, keep value
		y = y[y.find('[') + 1:]
		y = y[:y.find(']') - 1]

		# Convert from string to float
		y = float(y)

		# Keep the volume on 0% until ads end
		while METADATA['xesam:title'] == "Advertisement" or METADATA['xesam:title'] == "Spotify":
			try:
				METADATA = SPOTIFY_PROPERTIES.Get("org.mpris.MediaPlayer2.Player",	"Metadata")
			except:
				print("Spotify closed during an ad\nTurn volume up manually")
				exit()
			os.system('amixer -q -D pulse sset Master 0%')

		# Fade volume back
		fadeVolume(y)

	# If unpaused, timer restarts
	if n != 0:
		n = 0

	# Comment this out to loop program
	loop = "false"
